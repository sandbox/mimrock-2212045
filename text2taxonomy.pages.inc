<?php

/**
 * @file
 * Convert Text fields with select widget to taxonomy reference field page file.
 */

/**
 * Page callback of admin/structure/taxonomy/text2taxonomy.
 */
function text2taxonomy_convert() {
  return drupal_get_form('text2taxonomy_convert_form');
}

/**
 * Form callback for selecting a field to convert.
 */
function text2taxonomy_convert_form($form, &$form_state) {

  if (empty($form_state['values']['field'])) {
    $form['field_selector'] = array(
      '#prefix' => '<div id="text2taxonomy_field_selector_wrapper">',
      '#suffix' => '</div>',
    );
    $form['field_selector']['field'] = array(
      '#type' => 'select',
      '#title' => t('Field'),
      '#options' => _text2taxonomy_options_fields(),
      '#required' => TRUE,
      '#description' => t('Field to convert.'),
    );
    $form['field_selector']['vocabulary'] = array(
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#options' => _text2taxonomy_options_vocabularies(),
      '#required' => TRUE,
      '#description' => t('The taxonomy field to create will use this vocabulary.'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Convert')
    );
    return $form;
  }
  else {
    $form['field'] = array(
      '#type' => 'hidden',
      '#value' => $form_state['values']['field']
    );
    $form['vocabulary'] = array(
      '#type' => 'hidden',
      '#value' => $form_state['values']['vocabulary']
    );
    return confirm_form($form,
      t('This action will convert the %field field and its all instances to taxonomy term field. This action is irreversible. Do you wish to procceed?', array('%field' => $form_state['values']['field'])),
      'text2taxonomy/convert');
  }
}

/**
 * Submit callback for text2taxonomy_convert_form.
 */
function text2taxonomy_convert_form_submit($form, &$form_state) {
  if (!user_access('administer content types')) {
    drupal_access_denied();
  }
  if (empty($form_state['values']['confirm'])) {
    $form_state['rebuild'] = TRUE;
    return;
  }

  $field_name = $form_state['values']['field'];
  $vocabulary = taxonomy_vocabulary_load($form_state['values']['vocabulary']);

  $field_instances = array();
  $affected_entities = array();

  if (!_text2taxonomy_get_affected_data($field_name, $affected_entities, $field_instances)) {
    form_set_error('field_selector', t('Something went wrong. No changes in content or structure have been done.'));
    return;
  }
  $original_field_info = field_info_field($field_name);
  field_delete_field($field_name);
  _text2taxonomy_create_taxonomy_field($vocabulary, $field_name, $original_field_info);
  $term_map = _text2taxonomy_create_terms($vocabulary, $original_field_info);
  foreach ($field_instances as $instance) {
    _text2taxonomy_create_taxonomy_field_instance($instance);
  }
  foreach ($affected_entities as $entity_type => $entity_list) {
    foreach ($entity_list as $pid => $affected_entity) {
      // Exclude the anonymous user.
      if (!($entity_type == 'user' && $pid == 0)) {
        $new_entity = entity_load($entity_type, array($pid));
        if (is_array($new_entity)) {
          $new_entity = array_shift($new_entity);
        }
        $options = (array) field_get_items($entity_type, $affected_entity, $field_name);
        $new_entity->{$field_name} = array();
        $field_language = field_language($entity_type, $new_entity, $field_name);
        foreach ($options as $option) {
          $option = $option['value'];
          if (isset($term_map[$option])) {
            $new_entity->{$field_name}[$field_language][] = array('tid' => $term_map[$option]);
          }
        }
        entity_save($entity_type, $new_entity);
      }
    }
  }
  drupal_set_message(t('%field_name has been converted to taxonomy reference field', array('%field_name' => $field_name)), 'status');
}

/**
 * Get a list of installed fields in the system for a select list.
 *
 * @return array
 *   List of available fields as keys and values.
 */
function _text2taxonomy_options_fields() {
  $field_map = field_info_field_map();
  $ret = array();
  foreach ($field_map as $field_name => $field) {
    if ($field['type'] == 'list_text') {
      $ret[$field_name] = $field_name;
    }
  }
  return $ret;
}

/**
 * Get all installed vocabularies for a select list.
 *
 * @return array
 *   List of vocabularies in the system.
 *   vid => name
 */
function _text2taxonomy_options_vocabularies() {
  $vocs = taxonomy_get_vocabularies();
  $ret = array();
  foreach ($vocs as $voc) {
    $ret[$voc->vid] = $voc->name;
  }
  return $ret;
}

/**
 * Create a taxonomy field to replace a select list.
 *
 * @param object $vocabulary
 *   Vocabulary object.
 * @param string $field_name
 *   Name of the field.
 * @param array $field_info
 *   Field info of the original select field.
 * @return array
 *   The $field array with the id property filled in.
 */
function _text2taxonomy_create_taxonomy_field($vocabulary, $field_name, $field_info) {
  $field = array(
    'field_name' => $field_name,
    'type' => 'taxonomy_term_reference',
    'cardinality' => $field_info['cardinality'],
    'settings' => array(
      'allowed_values' => array(
        array(
          'vocabulary' => $vocabulary->machine_name,
          'parent' => 0
        ),
      ),
    ),
  );
  return field_create_field($field);
}

/**
 * Create a taxonomy field instance to replace a select list.
 * @param array $instance
 *   The instance array of the original select field.
 * @return array
 *   The $instance array with the id property filled in.
 */
function _text2taxonomy_create_taxonomy_field_instance($instance) {
  $instance['widget']['type'] = 'options_select';
  foreach ($instance['display'] as &$display) {
    if ($display['type'] == 'list_default') {
      // Update the display where the field is not hidden.
      $display['module'] = 'taxonomy';
      $display['type'] = 'taxonomy_term_reference';
    }
  }
  return field_create_instance($instance);
}

/**
 * Creates terms for the options of a select list. Every option is a new term in
 *   the vocabulary. If the lowercased, trimmed term matches an other term in the
 *   vocabulary, it will be used instead of creating a duplicate.
 * @param object $vocabulary
 *   Vocabulary object
 * @param array $field_info
 *   Field info of the original select field.
 * @return array
 *   A map which tells which option became which term.
 *   String key of select field => Term id
 */
function _text2taxonomy_create_terms($vocabulary, $field_info) {
  $ret = array();
  foreach ($field_info['settings']['allowed_values'] as $key => $value) {

    $terms = taxonomy_get_term_by_name($value, $vocabulary->machine_name);
    if (empty($terms)) {
      $term = new stdClass();
      $term->name = check_plain($value);
      $term->vid = $vocabulary->vid;
      taxonomy_term_save($term);
    }
    else {
      // It is hard to decide which one is the best match, so we use the first.
      $term = array_shift($terms);
    }
    $ret[$key] = $term->tid;
  }
  return $ret;
}

/**
 * Get affected data.
 * @param $field_name
 * @param array $affected_entities
 * @param array $field_instances
 * @return boolean
 * @todo document
 */
function _text2taxonomy_get_affected_data($field_name, array &$affected_entities, array &$field_instances) {
  $field_map = field_info_field_map();
  $bundle_map = $field_map[$field_name]['bundles'];

  foreach ($bundle_map as $entity_type => $bundles) {
    $affected_entities[$entity_type] = array();
    foreach ($bundles as $bundle) {
      $entity_info = entity_get_info($entity_type);
      // If there are bundles, we only have to load entities from only this bundle.
      if (empty($entity_info['bundle keys'])) {
        $affected_entities[$entity_type] += entity_load($entity_type, FALSE);
      }
      else {
        $entity_load = entity_load($entity_type, FALSE, array($entity_info['bundle keys']['bundle'] => $bundle));
        if (!array($entity_load)) {
          return FALSE;
        }
        $affected_entities[$entity_type] += $entity_load;
      }
      $field_read_instances = field_read_instance($entity_type, $field_name, $bundle);
      if (!array($field_read_instances)) {
        return FALSE;
      }
      $field_instances[] = $field_read_instances;
    }
  }
  return TRUE;
}
